/*
SQLyog Community v11.42 (64 bit)
MySQL - 5.6.36 : Database - devrantdb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`devrantdb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `devrantdb`;

/*Table structure for table `ad_module_mst` */

DROP TABLE IF EXISTS `ad_module_mst`;

CREATE TABLE `ad_module_mst` (
  `moduleid` varchar(100) DEFAULT NULL,
  `modulename` varchar(100) DEFAULT NULL,
  `logopath` varchar(200) DEFAULT NULL,
  `colorclass` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ad_module_mst` */

insert  into `ad_module_mst`(`moduleid`,`modulename`,`logopath`,`colorclass`) values ('01','Sales','fa fa-camera-retro','menu-magenta'),('02','Inventory','fa fa-code','menu-blue'),('03','Manufacturing','fa fa-comment','menu-green'),('04','Administration','fa fa-plane','menu-yellow');

/*Table structure for table `ad_right_mst` */

DROP TABLE IF EXISTS `ad_right_mst`;

CREATE TABLE `ad_right_mst` (
  `ADRM_RIGHT_ID` varchar(100) DEFAULT NULL,
  `ADRM_NAME` varchar(200) DEFAULT NULL,
  `ADRM_PAGE_NAME` varchar(200) DEFAULT NULL,
  `ADRM_MODULE_ID` varchar(200) DEFAULT NULL,
  `ADRM_TYPE` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ad_right_mst` */

insert  into `ad_right_mst`(`ADRM_RIGHT_ID`,`ADRM_NAME`,`ADRM_PAGE_NAME`,`ADRM_MODULE_ID`,`ADRM_TYPE`) values ('1','Sale Order','SaSoHdrAddEdit','01','T'),('2','Despatch','SaDesHdr','01','T'),('3','Invoice','SaInvHdr','01','T'),('4','return','sareturn','01','T'),('5','Party Master','AcpartyMst','01','M'),('6','Item Master','InItmMst','02','M'),('7','Item Listing','InItmRep','01','R'),('8','Party Listing','AcpartyRep','01','R'),('9','GRN Printing','AcpartyRep','01','R'),('10','manual purchase order','AcpartyRep','02','T'),('11','item listing','AcpartyRep','02','R');

/*Table structure for table `ad_user_mst` */

DROP TABLE IF EXISTS `ad_user_mst`;

CREATE TABLE `ad_user_mst` (
  `aum_user` varchar(100) NOT NULL,
  `aum_pass` varchar(200) NOT NULL,
  `aum_email` varchar(100) NOT NULL,
  `aum_image_path` varchar(100) DEFAULT '/resources/images/avatars/user.jpg',
  `aum_image` blob,
  `aum_name` varchar(50) DEFAULT NULL,
  `aum_middle` varchar(50) DEFAULT NULL,
  `aum_dob` date DEFAULT NULL,
  `aum_gender` char(1) DEFAULT NULL,
  `aum_comments` varchar(500) DEFAULT NULL,
  `aum_website` varchar(200) DEFAULT NULL,
  `aum_phone` varchar(12) DEFAULT NULL,
  `aum_facebook` varchar(200) DEFAULT NULL,
  `aum_twiter` varchar(200) DEFAULT NULL,
  `aum_google` varchar(200) DEFAULT NULL,
  `aum_publicprofile` char(1) DEFAULT NULL,
  `aum_newupdate` char(1) DEFAULT NULL,
  `aum_keephistory` char(1) DEFAULT NULL,
  PRIMARY KEY (`aum_user`,`aum_email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ad_user_mst` */

insert  into `ad_user_mst`(`aum_user`,`aum_pass`,`aum_email`,`aum_image_path`,`aum_image`,`aum_name`,`aum_middle`,`aum_dob`,`aum_gender`,`aum_comments`,`aum_website`,`aum_phone`,`aum_facebook`,`aum_twiter`,`aum_google`,`aum_publicprofile`,`aum_newupdate`,`aum_keephistory`) values ('piyush','2z0UN8iMRLH7gC1BvtL2tg==','pyadav@microproindia.com','/resources/images/avatars/user.jpg',NULL,'Peeyush','Yadav',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('tanu','iaJkPaOh+9AMST2zCqVqYw==','tanu@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
