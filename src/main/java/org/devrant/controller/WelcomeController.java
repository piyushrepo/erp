package org.devrant.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.devrant.dao.FetchRightMaster;
import org.devrant.model.RightMaster;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class WelcomeController {
	@Autowired
	FetchRightMaster rightmaster; 
	List<RightMaster> rightlist = null;
	

	@RequestMapping(value = "/welcomepage", method = RequestMethod.POST)
	public @ResponseBody List<RightMaster> doLogin(HttpServletRequest request, HttpServletResponse response) {
		
			switch (request.getParameter("type")) {
			case "M":
				rightlist = rightmaster.getRightMst("0"+request.getParameter("mod"), "M");
				break;
			case "T":
				rightlist = rightmaster.getRightMst("0"+request.getParameter("mod"), "T");
				break;
			case "R":
				rightlist = rightmaster.getRightMst("0"+request.getParameter("mod"), "R");
				break;
			}
			
	
		return rightlist;
	}

}
