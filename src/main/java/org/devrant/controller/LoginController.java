package org.devrant.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.devrant.dao.UserAthenticate;
import org.devrant.model.Modules;
import org.devrant.model.RegisterUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@ComponentScan("org.devrant")
public class LoginController {
	@Autowired
	private UserAthenticate ua ;
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView doLogin(HttpServletRequest request) throws ParseException {
		ModelAndView mv=new ModelAndView();
		List<Modules> moduleslist=ua.getModules();
		RegisterUser userobj=new RegisterUser();
		userobj=ua.doAuthentication(request.getParameter("username"),request.getParameter("password"));
		request.getSession().setAttribute("userobj", userobj);
		request.getSession().setAttribute("modules", moduleslist);
		if (userobj!=null) {
			mv.setViewName("redirect:/welcome");	
		} else {
			mv.setViewName("index");
		}
		return mv;
	}

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public ModelAndView doLoginWithGet() {
		ModelAndView mv=new ModelAndView();
		mv.setViewName("/welcome");
		return mv;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logoutPage(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		mv.setViewName("index");
		return mv;// You can redirect wherever you want, but generally it's a
					// good practice to show login screen again.
	}
}
