package org.devrant.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.devrant.dao.UserRegistration;
import org.devrant.encryption.AesEncryption;
import org.devrant.model.RegisterUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@MultipartConfig
@Controller
public class RegisterController {
@Autowired
private UserRegistration userregistration;
private SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
	@RequestMapping(value ="/Register",method = RequestMethod.POST)
	public ModelAndView doRegister(@RequestParam(name = "username1") String uname,
		@RequestParam(name = "ConfirmPassword") String pass,@RequestParam(name = "email") String email) {
		userregistration.doRegistration(uname,pass,email);
		ModelAndView mv = new ModelAndView();
		mv.setViewName("redirect:/index");
		return mv;
	}
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView doRegisterWithGet() {
		ModelAndView mv=new ModelAndView();
		mv.setViewName("/index");
		return mv;
	}
	
	
	@RequestMapping(value ="/UpdateUser",method = RequestMethod.POST)
	public ModelAndView doUpdateUserDetails(HttpServletRequest request, HttpServletResponse response,@RequestParam("imagefile") MultipartFile imagefile) throws ParseException, Exception {
		RegisterUser registeruser=new RegisterUser();
		registeruser.setAum_user(String.valueOf(request.getParameter("userid")));
		registeruser.setAum_name(String.valueOf(request.getParameter("fname")));
		registeruser.setAum_middle(String.valueOf(request.getParameter("lname")));
		registeruser.setAum_email(String.valueOf(request.getParameter("email")));
		registeruser.setAum_gender(String.valueOf(request.getParameter("gender")));
		registeruser.setAum_website(String.valueOf(request.getParameter("website")));
		registeruser.setAum_phone(String.valueOf(request.getParameter("phone")));
		registeruser.setAum_facebook(String.valueOf(request.getParameter("facebook")));
		registeruser.setAum_twiter(String.valueOf(request.getParameter("twitter")));
		registeruser.setAum_google(String.valueOf(request.getParameter("google")));	
		registeruser.setAum_comments(String.valueOf(request.getParameter("comment")));
		registeruser.setAum_pass(String.valueOf(request.getParameter("confirmpass")));
		registeruser.setAum_dob(sdf.parse(request.getParameter("dateofbirth").toString()));
		registeruser.setAum_publicprofile(String.valueOf(request.getParameter("profilepublic")));
		registeruser.setAum_newupdate(String.valueOf(request.getParameter("newupdates")));
		registeruser.setAum_keephistory(String.valueOf(request.getParameter("historyconversation")));
		System.out.println("comfirm passowrd"+request.getParameter("confirmpass"));
		System.out.println("comfirm passowrd"+request.getParameter("confirmpass").length());
		if(request.getParameter("confirmpass").length()!=0){
			System.out.println("came inside here");
		    registeruser.setAum_pass(AesEncryption.encrypt(String.valueOf(request.getParameter("confirmpass")),String.valueOf(request.getParameter("userid"))));
		}
		if (!imagefile.isEmpty()) {
			 BufferedImage src = ImageIO.read(new ByteArrayInputStream(imagefile.getBytes()));
			 File destination = new File("C:\\Users\\pyadav.MICROPROINDIA\\workspace\\ERP\\src\\main\\webapp\\resources\\images\\"+String.valueOf(request.getParameter("email"))+"_"+imagefile.getOriginalFilename()); 
			 ImageIO.write(src, "png", destination);
			 registeruser.setAum_image_path("C:\\Users\\pyadav.MICROPROINDIA\\workspace\\ERP\\src\\main\\webapp\\resources\\images\\"+String.valueOf(request.getParameter("email"))+"_"+imagefile.getOriginalFilename());
		}
		userregistration.doUpdateRegistration(registeruser);
		request.getSession().setAttribute("userobj", registeruser);
		ModelAndView mv = new ModelAndView();
		mv.setViewName("redirect:/welcome");
		return mv;
	}
	
	
	
	@RequestMapping(value="/checkEmail",method=RequestMethod.POST)
	public @ResponseBody String checkEmail(HttpServletRequest request, HttpServletResponse response) {
		return userregistration.doCheckEmail(request.getParameter("email"));
	}
	
	@RequestMapping(value="/checkUserid",method=RequestMethod.POST)
	public @ResponseBody String checkUserid(HttpServletRequest request, HttpServletResponse response) {
		return userregistration.doCheckUserid(request.getParameter("user"));
	}
}
