package org.devrant.util;

import org.devrant.encryption.AesEncryption;
import org.devrant.model.RegisterUser;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DatabaseUtils {
	public static <T> Session doSession(Class<T> cls)
	{
		
		Configuration cfg = new Configuration().configure().addAnnotatedClass(cls);
		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		return session;
		
	}
	
	public static <T> Criteria createCriteriaQuery(Class<T> cls,Session session)
	{
		org.hibernate.Transaction t = session.beginTransaction();
		Criteria cr = session.createCriteria(cls);
		return cr;
		
	}
}
