package org.devrant.dao;

import java.util.List;
import org.devrant.encryption.AesEncryption;
import org.devrant.model.Modules;
import org.devrant.model.RegisterUser;
import org.devrant.util.DatabaseUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
@Repository
public class UserAthenticate {
	public RegisterUser doAuthentication(String uname, String pass) {
		Session session=DatabaseUtils.doSession(RegisterUser.class);
		Criteria cr = DatabaseUtils.createCriteriaQuery(RegisterUser.class, session);
		String encrptedpass = AesEncryption.encrypt(pass, uname);
		cr.add(Restrictions.eq("aum_user", uname));
		cr.add(Restrictions.eq("aum_pass", encrptedpass));
		RegisterUser user= (RegisterUser) cr.uniqueResult();
	    session.close();
	    if (user==null) {
			return null;
		} else {
			return user;

		}
		
	}
	public List<Modules> getModules()
	{
		Session session=DatabaseUtils.doSession(Modules.class);
		org.hibernate.Transaction t = session.beginTransaction();
		//query for modules
		List<Modules> list = session.createCriteria(Modules.class).list();
        t.commit();
        session.close();
        return list;
	}
	
	
	
	
}
