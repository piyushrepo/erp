package org.devrant.dao;

import javax.servlet.annotation.MultipartConfig;

import org.devrant.encryption.AesEncryption;
import org.devrant.model.Modules;
import org.devrant.model.RegisterUser;
import org.devrant.util.DatabaseUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
@MultipartConfig
@Repository
public class UserRegistration {
	
public void doRegistration(String username, String pass,String email)
{
	Session session=DatabaseUtils.doSession(RegisterUser.class);
	org.hibernate.Transaction t = session.beginTransaction();
	String encrptedpass = AesEncryption.encrypt(pass, username);
	//Add new Employee object
	 RegisterUser usr = new RegisterUser();
    usr.setAum_user(username);
    usr.setAum_pass(encrptedpass);
    usr.setAum_email(email); 
    //Save the employee in database
    session.save(usr);
    //Commit the transaction
    t.commit();
    session.close();
    
}

public void doUpdateRegistration(RegisterUser registeruser)
{
	
	Session session=DatabaseUtils.doSession(RegisterUser.class);
	org.hibernate.Transaction t = session.beginTransaction();
    //update the employee in database
	RegisterUser regusr =  (RegisterUser) session.load(RegisterUser.class,registeruser.getAum_user().toString());
	regusr.setAum_website(registeruser.getAum_website().toString());
	regusr.setAum_gender(registeruser.getAum_gender().trim());
	regusr.setAum_phone(registeruser.getAum_phone().trim());
	regusr.setAum_name(registeruser.getAum_name().trim());
	regusr.setAum_middle(registeruser.getAum_middle().trim());
	regusr.setAum_facebook(registeruser.getAum_facebook());
	regusr.setAum_twiter(registeruser.getAum_twiter());
	regusr.setAum_google(registeruser.getAum_google());
	regusr.setAum_pass(registeruser.getAum_pass());
	regusr.setAum_comments(registeruser.getAum_comments());
	regusr.setAum_dob(registeruser.getAum_dob());
	regusr.setAum_publicprofile(registeruser.getAum_publicprofile().trim());
	regusr.setAum_newupdate(registeruser.getAum_newupdate().trim());
	regusr.setAum_keephistory(registeruser.getAum_keephistory().trim());
	regusr.setAum_image_path(registeruser.getAum_image_path());
	session.save(regusr);
    //Commit the transaction
    t.commit();
    session.close();
    
    
}

public String doCheckEmail(String  email)
{
    
	Session session=DatabaseUtils.doSession(RegisterUser.class);
	Criteria cr = DatabaseUtils.createCriteriaQuery(RegisterUser.class, session);
	cr.add(Restrictions.eq("aum_email", email));
	RegisterUser user= (RegisterUser) cr.uniqueResult();
    session.close();
    if (user==null) {
		return "false";
	} else {
		return "true";

	}
}

public String doCheckUserid(String  userid)
{
    
	Session session=DatabaseUtils.doSession(RegisterUser.class);
	Criteria cr = DatabaseUtils.createCriteriaQuery(RegisterUser.class, session);
	cr.add(Restrictions.eq("aum_user", userid));
	RegisterUser user= (RegisterUser) cr.uniqueResult();
    session.close();
    if (user==null) {
		return "false";
	} else {
		return "true";

	}
}

}
