package org.devrant.dao;

import java.util.ArrayList;
import java.util.List;

import org.devrant.model.Modules;
import org.devrant.model.RegisterUser;
import org.devrant.model.RightMaster;
import org.devrant.util.DatabaseUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
@Repository
public class FetchRightMaster {
public List<RightMaster> getRightMst(String moduleid,String type)
{
	
	Session session=DatabaseUtils.doSession(RightMaster.class);
	
	Criteria cr = session.createCriteria(RightMaster.class);
	cr.add(Restrictions.eq("ADRM_MODULE_ID", moduleid));
	cr.add(Restrictions.eq("ADRM_TYPE", type));
	List <RightMaster> rightmasterlist= cr.list();
   
    session.close();
	return rightmasterlist;
}
}
