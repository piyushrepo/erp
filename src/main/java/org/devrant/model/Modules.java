package org.devrant.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "ad_module_mst")
@Entity
public class Modules {

	String moduleid;
	@Id
	String modulename;
	String colorclass;

	public String getLogopath() {
		return logopath;
	}

	public void setLogopath(String logopath) {
		this.logopath = logopath;
	}

	String logopath;

	public String getModuleid() {
		return moduleid;
	}

	public String getColorclass() {
		return colorclass;
	}

	public void setColorclass(String colorclass) {
		this.colorclass = colorclass;
	}

	public void setModuleid(String moduleid) {
		this.moduleid = moduleid;
	}

	public String getModulename() {
		return modulename;
	}

	public void setModulename(String modulename) {
		this.modulename = modulename;
	}

}
