package org.devrant.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="ad_right_mst")
public class RightMaster {
@Id	
String ADRM_RIGHT_ID;
String ADRM_NAME;
String ADRM_PAGE_NAME;
String ADRM_MODULE_ID;
public String getADRM_RIGHT_ID() {
	return ADRM_RIGHT_ID;
}
public void setADRM_RIGHT_ID(String aDRM_RIGHT_ID) {
	ADRM_RIGHT_ID = aDRM_RIGHT_ID;
}
public String getADRM_NAME() {
	return ADRM_NAME;
}
public void setADRM_NAME(String aDRM_NAME) {
	ADRM_NAME = aDRM_NAME;
}
public String getADRM_PAGE_NAME() {
	return ADRM_PAGE_NAME;
}
public void setADRM_PAGE_NAME(String aDRM_PAGE_NAME) {
	ADRM_PAGE_NAME = aDRM_PAGE_NAME;
}
public String getADRM_MODULE_ID() {
	return ADRM_MODULE_ID;
}
public void setADRM_MODULE_ID(String aDRM_MODULE_ID) {
	ADRM_MODULE_ID = aDRM_MODULE_ID;
}
public String getADRM_TYPE() {
	return ADRM_TYPE;
}
public void setADRM_TYPE(String aDRM_TYPE) {
	ADRM_TYPE = aDRM_TYPE;
}
String ADRM_TYPE;
}
