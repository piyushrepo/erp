package org.devrant.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
@Table(name="ad_user_mst")
@Entity
public class RegisterUser {
	@Id	
	private String aum_user;
	@Column(nullable=false)
	private String aum_pass;
	private String aum_image;
	private String aum_image_path;
	private String aum_name;
	private String aum_middle;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private Date aum_dob;
	private String aum_gender;
	private String aum_comments;
	public String getAum_image_path() {
		return aum_image_path;
	}
	public void setAum_image_path(String aum_image_path) {
		this.aum_image_path = aum_image_path;
	}
	public String getAum_name() {
		return aum_name;
	}
	public void setAum_name(String aum_name) {
		this.aum_name = aum_name;
	}
	public String getAum_middle() {
		return aum_middle;
	}
	public void setAum_middle(String aum_middle) {
		this.aum_middle = aum_middle;
	}
	public Date getAum_dob() {
		return aum_dob;
	}
	public void setAum_dob(Date aum_dob) {
		this.aum_dob = aum_dob;
	}
	public String getAum_gender() {
		return aum_gender;
	}
	public void setAum_gender(String aum_gender) {
		this.aum_gender = aum_gender;
	}
	public String getAum_comments() {
		return aum_comments;
	}
	public void setAum_comments(String aum_comments) {
		this.aum_comments = aum_comments;
	}
	public String getAum_website() {
		return aum_website;
	}
	public void setAum_website(String aum_website) {
		this.aum_website = aum_website;
	}
	public String getAum_phone() {
		return aum_phone;
	}
	public void setAum_phone(String aum_phone) {
		this.aum_phone = aum_phone;
	}
	public String getAum_facebook() {
		return aum_facebook;
	}
	public void setAum_facebook(String aum_facebook) {
		this.aum_facebook = aum_facebook;
	}
	public String getAum_twiter() {
		return aum_twiter;
	}
	public void setAum_twiter(String aum_twiter) {
		this.aum_twiter = aum_twiter;
	}
	public String getAum_google() {
		return aum_google;
	}
	public void setAum_google(String aum_google) {
		this.aum_google = aum_google;
	}
	public String getAum_publicprofile() {
		return aum_publicprofile;
	}
	public void setAum_publicprofile(String aum_publicprofile) {
		this.aum_publicprofile = aum_publicprofile;
	}
	public String getAum_newupdate() {
		return aum_newupdate;
	}
	public void setAum_newupdate(String aum_newupdate) {
		this.aum_newupdate = aum_newupdate;
	}
	public String getAum_keephistory() {
		return aum_keephistory;
	}
	public void setAum_keephistory(String aum_keephistory) {
		this.aum_keephistory = aum_keephistory;
	}
	private String aum_website;
	private String aum_phone;
	private String aum_facebook;
	private String aum_twiter;
	private String aum_google;
	private String aum_publicprofile;
	private String aum_newupdate;
	private String aum_keephistory;
	public String getAum_user() {
		return aum_user;
	}
	public void setAum_user(String aum_user) {
		this.aum_user = aum_user;
	}
	public String getAum_pass() {
		return aum_pass;
	}
	public void setAum_pass(String aum_pass) {
		this.aum_pass = aum_pass;
	}
	public String getAum_image() {
		return aum_image;
	}
	public void setAum_image(String aum_image) {
		this.aum_image = aum_image;
	}
	public String getAum_email() {
		return aum_email;
	}
	public void setAum_email(String aum_email) {
		this.aum_email = aum_email;
	}
	private String aum_email;
	
}
